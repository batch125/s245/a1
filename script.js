//users

db.users.insertMany([
	{	"firstName": "Diane",
		"lastName": "Murphy",
		"Email":"dmurphy@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{	"firstName":"Mary",
		"lastName": "Patterson",
		"Email": "mpatterson@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{	"firstName": "Jeff",
		"lastName": "Firrelli",
		"Email": "jfirrelli@mail.com",
		"isAdmin": false,
		"isActive": true 
	},
	{	"firstName": "Gerard",
		"lastName": "Bondur",
		"Email": "gbondur@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{	"firstName": "Pamela",
		"lastName": "Castillo",
		"Email": "pcastillo@mail.com",
		"isAdmin": true,
		"isActive": false
	},
	{	"firstName": "George",
		"lastName": "Vanauf",
		"Email": "gvanauf@mail.com",
		"isAdmin": true,
		"isActive": true
	}
	]);

//course

db.course.insertMany([
	{	"name": "Professional Development",
		"Price": "10000"
	},
	{	"name": "Business Processing",
		"Price": "13000"
	}
	]);

//Murphy, Firrelli
db.course.updateOne(
	{"name":"Professional Development"},
	{ $set: 
            {"Enrollees" : 
                ["6125b3b55139757117238d28", 
                 "6125b3b55139757117238d26"]
                }
});
//Patterson, Bondur
db.course.updateOne(
	{"name":"Business Processing"},
	{ $set: 
            {"Enrollees" : 
                ["6125b3b55139757117238d29", 
                 "6125b3b55139757117238d27"]
                }
});

//CRUD

//not admin
db.users.find({"isAdmin":false})
